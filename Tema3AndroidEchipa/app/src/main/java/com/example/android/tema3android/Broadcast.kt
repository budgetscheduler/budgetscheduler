package com.example.tema3android

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.android.tema3android.R

class Broadcast: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val builder : NotificationCompat.Builder = NotificationCompat.Builder(context!!, "notify")
            .setContentTitle("Alarm").setContentText("You have an alarm set at this hour").setSmallIcon(
                R.drawable.ic_launcher_background)
        val notificationManager: NotificationManagerCompat = NotificationManagerCompat.from(context)
        notificationManager.notify(200, builder.build())
    }
}