package com.example.android.tema3android

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.tema3android.Broadcast
import com.example.tema3android.Fragment_Date
import kotlinx.android.synthetic.main.fragment_one.*
import java.util.*

class FragmentOne : Fragment() {

    companion object {
        fun newInstance() = FragmentOne()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_one, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createNotificationChannel()
        btn1.setOnClickListener {
            addFragmentTimePick()
        }
        btn2.setOnClickListener {
            addFragmentDatePick()
        }
        btn3.setOnClickListener {
            Toast.makeText(activity, "Alarm set!", Toast.LENGTH_SHORT).show()
            val intent = Intent(activity, Broadcast::class.java)
            val pendingIntent = PendingIntent.getBroadcast(activity, 0, intent, 0)
            val alarmManager: AlarmManager = activity?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    private fun addFragmentTimePick() {

        val fragment = Fragment_Time.newInstance()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.main_FrameLayout, fragment, "Fragment_Time")
        transaction?.hide(this)
        transaction?.addToBackStack("Fragment_Time")
        transaction?.commit()
    }
    private fun addFragmentDatePick() {

        val fragment = Fragment_Date.newInstance()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.main_FrameLayout, fragment, "Fragment_Date")
        transaction?.hide(this)
        transaction?.addToBackStack("Fragment_Date")
        transaction?.commit()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val channel: NotificationChannel = NotificationChannel("notify", "Channel", NotificationManager.IMPORTANCE_DEFAULT)

        val notificationManager: NotificationManager = activity?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

}