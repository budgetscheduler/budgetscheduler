package com.example.android.budgetscheduler.DataBase

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.android.budgetscheduler.DataBase.Expense

@Dao
interface ExpenseDao {
    @Query("SELECT * FROM expense")
    fun all(): List<Expense>

    @Query("SELECT * FROM expense WHERE uid IN (:userIds)")
    fun loadAllByIds(userIds: IntArray?): List<Expense?>?

    @Query(
        "SELECT * FROM expense WHERE Category LIKE :category"
    )
    fun findByCategory(category: String?): Expense?

    @Insert
    fun insertAll(expense: Expense?)

    @Delete
    fun delete(expense: Expense?)
}