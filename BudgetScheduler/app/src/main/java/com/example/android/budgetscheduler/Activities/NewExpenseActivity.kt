package com.example.android.budgetscheduler.Activities

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock
import android.view.View
import android.widget.*
import com.example.android.budgetscheduler.Fragments.SecondFragment
import com.example.android.budgetscheduler.DataBase.AppDatabase
import com.example.android.budgetscheduler.DataBase.Expense
import com.example.android.budgetscheduler.R
import kotlinx.android.synthetic.main.activity_new_expense.*

class NewExpenseActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    lateinit var expenses1: List<Expense>
    var expenses2: ArrayList<Expense> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_expense)

        // Get the Intent that started this activity and extract the string
        val message = intent.getStringExtra(AlarmClock.EXTRA_MESSAGE)

        // Capture the layout's TextView and set the string as its text
        val textView = findViewById<TextView>(R.id.titlu).apply {
            text = message
        }
        val spinner: Spinner = findViewById(R.id.spinner)
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this,
            R.array.categorii,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter

            overridePendingTransition(
                R.anim.slide_in_left,
                R.anim.slide_out_left
            )

        }
    }

    fun AddExpense(view: View) {
        val suma = Integer.parseInt(sumaCheltuita.text.toString())
        val scop = scopCheltuiala.text.toString()
        val categorie = spinner.selectedItem.toString()
        if (suma == null || scop == "" || categorie == "") {
            val toast = Toast.makeText(
                applicationContext,
                "Toate campurile sunt obligatorii",
                Toast.LENGTH_SHORT
            )
            toast.show()
        } else {
            var expense = Expense(
                scop,
                suma,
                categorie
            )
            expenses2.add(expense)
            AsyncTaskInsert().execute(expense)
            supportFragmentManager.beginTransaction().show(SecondFragment()).commit()
            finish();
        }
    }

    inner class AsyncTaskInsert() : AsyncTask<Expense, Void, Void>() {
        override fun doInBackground(vararg params: Expense?): Void? {
            AppDatabase.getAppDatabase(this@NewExpenseActivity!!).expenseDao().insertAll(params[0])
            return null
        }
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
        val spinner: Spinner = findViewById(R.id.spinner)
        spinner.onItemSelectedListener = this
    }

    override fun onNothingSelected(parent: AdapterView<*>) {
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(
            R.anim.slide_in_left,
            R.anim.slide_out_left
        )

    }
}
