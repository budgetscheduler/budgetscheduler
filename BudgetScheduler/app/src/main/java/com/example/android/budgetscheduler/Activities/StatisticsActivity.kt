package com.example.android.budgetscheduler.Activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.android.budgetscheduler.Fragments.StatisticsFragment
import com.example.android.budgetscheduler.R

class StatisticsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistics)
        supportFragmentManager.beginTransaction().add(R.id.FragmentStatistics, StatisticsFragment()).commit()
        overridePendingTransition(
            R.anim.slide_in_left,
            R.anim.slide_out_left
        )
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(
            R.anim.slide_in_left,
            R.anim.slide_out_left
        )
    }
}
