package com.example.android.budgetscheduler.DataBase

import androidx.fragment.app.FragmentActivity
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Expense::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun expenseDao(): ExpenseDao
    //abstract fun budgetDao(): BudgetDao

    companion object {
        private var mInstance: AppDatabase? = null

        fun getAppDatabase(context: FragmentActivity): AppDatabase {
            if (mInstance == null) {
                mInstance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "expense_database"
                ).build()
            }
            return mInstance as AppDatabase
        }
    }

}