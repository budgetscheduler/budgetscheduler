package com.example.android.budgetscheduler.Activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.android.budgetscheduler.R

class NewIncomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_income)
        overridePendingTransition(
            R.anim.slide_in_left,
            R.anim.slide_out_left
        )
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(
            R.anim.slide_in_left,
            R.anim.slide_out_left
        )

    }

}