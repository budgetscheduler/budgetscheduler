package com.example.android.budgetscheduler.Activities

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.android.budgetscheduler.Fragments.SecondFragment
import com.example.android.budgetscheduler.DataBase.Budget
import com.example.android.budgetscheduler.R
import kotlinx.android.synthetic.main.activity_new_budget.*
import java.util.*

class NewBudgetActivity : AppCompatActivity() {
    var budgets: ArrayList<Budget> = ArrayList()
    var budget = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_budget)
        overridePendingTransition(
            R.anim.slide_in_left,
            R.anim.slide_out_left
        )
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(
            R.anim.slide_in_left,
            R.anim.slide_out_left
        )
    }

    fun AddBudget(view: View) {
        val suma = Integer.parseInt(editTextSuma.text.toString())
        if (suma == null) {
            val toast = Toast.makeText(
                applicationContext,
                "Introduceti o suma",
                Toast.LENGTH_SHORT
            )
            toast.show()
        } else {
//            val dateFormat: DateFormat = SimpleDateFormat("MM")
//            val date = Date()
//            Log.d("Month", dateFormat.format(date))
//            var budget = Budget(suma, date.toString())
//            budgets.add(budget)
//            AsyncTaskInsert().execute(budget)
            budget = suma
            supportFragmentManager.beginTransaction().show(SecondFragment()).commit()
            finish();
        }
    }

//    inner class AsyncTaskInsert() : AsyncTask<Budget, Void, Void>() {
//        override fun doInBackground(vararg params: Budget?): Void? {
//            AppDatabase.getAppDatabase(this@NewBudgetActivity!!).budgetDao().insertAll(params[0])
//            return null
//        }
//    }
}