package com.example.android.budgetscheduler.Fragments

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android.budgetscheduler.Activities.NewIncomeActivity
import com.example.android.budgetscheduler.Activities.StatisticsActivity
import com.example.android.budgetscheduler.DataBase.Adapter
import kotlinx.android.synthetic.main.fragment_second.*
import com.example.android.budgetscheduler.DataBase.AppDatabase
import com.example.android.budgetscheduler.DataBase.Expense
import com.example.android.budgetscheduler.R
import kotlin.collections.ArrayList

class SecondFragment : Fragment() {
    var button: Button? = null
    var buttonS: Button?= null
    //private var tracker
    var recyclerView: RecyclerView? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_second, container, false)
        recyclerView = view.findViewById(R.id.recycler_view)
        button = view.findViewById(R.id.button3) as Button
        button?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                startActivity(Intent(activity, NewIncomeActivity::class.java))
            }
        })

        buttonS = view.findViewById(R.id.statistici)
        buttonS?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                startActivity(Intent(activity, StatisticsActivity::class.java))
            }
        })


        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        AppDatabase.getAppDatabase(activity!!)
        recycler_view.layoutManager = LinearLayoutManager(activity)
        getExpenses()

    }

/*    private fun setupTracker() {
//        tracker = recyclerView?.let {
//            SelectionTracker.Builder<Long>(
//                "mySelection",
//                it,
//                StableIdKeyProvider(recyclerView!!),
//                MyItemDetailsLookup(recyclerView!!),
//                StorageStrategy.createLongStorage()
//            ).withSelectionPredicate(
//                SelectionPredicates.createSelectAnything()
//            ).build()
//        }
//
//        tracker?.addObserver(
//            object : SelectionTracker.SelectionObserver<Long>() {
//                override fun onSelectionChanged() {
//                    super.onSelectionChanged()
//                    val nItems: Int? = tracker?.selection!!.size()
//                        DeleteExpense(tracker?.selection!!)
//                }
//            })
//
//        adapter.tracker = tracker
// */

    private fun getExpenses() {
        var expenses2: ArrayList<Expense> = ArrayList()
        AsyncTask.execute {
            expenses2.addAll(AppDatabase.getAppDatabase(activity!!).expenseDao().all())
        }
        recyclerView?.adapter =
            Adapter(expenses2)
    }
}
//    private fun DeleteExpense(selection: Selection<Long>) {
//
//        var expense : Expense? = null
//
////        if(expenses2.isEmpty()){
////            Toast.makeText(context, "List is empty!", Toast.LENGTH_SHORT).show()
////            return
////        }
//
//        val list = selection.map {
//            adapter.list[it.toInt()]
//        }.toList()
//
//        for (i in 0 until list.size)
//        {
//            expenses2.removeAt(i)
//            expense = expenses2[i]
//            AsyncTaskDelete().execute(expense)
//        }
//    }
//
//    @SuppressLint("StaticFieldLeak")
//    inner class AsyncTaskDelete() : AsyncTask<Expense, Void, Void>() {
//        override fun doInBackground(vararg params: Expense?): Void? {
//            AppDatabase.getAppDatabase(activity!!).expenseDao().delete(params[0])
//            return null
//        }
//    }
