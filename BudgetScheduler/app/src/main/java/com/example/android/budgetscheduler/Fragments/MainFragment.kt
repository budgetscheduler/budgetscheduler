package com.example.android.budgetscheduler.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.android.budgetscheduler.Activities.AlarmActivity
import com.example.android.budgetscheduler.Activities.NewBudgetActivity
import com.example.android.budgetscheduler.Activities.SecondActivity
import com.example.android.budgetscheduler.R


class MainFragment : Fragment() {
    var button1: Button? = null
    var button2: Button? = null
    var button3: Button? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_main, container, false)

        button1 = view.findViewById(R.id.btn_go_to_second_activity) as Button
        button1?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                val intent = Intent(view!!.context, SecondActivity::class.java)
                view!!.context.startActivity(intent)
            }
        })

        button2 = view.findViewById(R.id.btn_go_to_add_new_income) as Button
        button2?.setOnClickListener { view ->
            val intent2 = Intent(view!!.context, NewBudgetActivity::class.java)
            view!!.context.startActivity(intent2)
        }

        button3 = view.findViewById(R.id.btn_go_to_set_alarm) as Button
        button3?.setOnClickListener { view ->
            val intent = Intent(view!!.context, AlarmActivity::class.java)
            view!!.context.startActivity(intent)
        }
        return view
    }

}