package com.example.android.budgetscheduler.DataBase

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.android.budgetscheduler.DataBase.Budget

@Dao
interface BudgetDao {
    @Query("SELECT * FROM budget")
    fun all(): List<Budget>

    @Query("SELECT * FROM budget WHERE uid IN (:budgetIds)")
    fun loadAllByIds(budgetIds: IntArray?): List<Budget>?

    @Query(
        "SELECT * FROM budget WHERE month LIKE :month"
    )
    fun findByMonth(month: String?): Budget?

    @Insert
    fun insertAll(budget: Budget?)

    @Delete
    fun delete(budget: Budget?)
}