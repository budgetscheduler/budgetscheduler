package com.example.android.budgetscheduler.Fragments

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.AlarmClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.android.budgetscheduler.R

class AlarmFragment : Fragment() {
    var mHourEditText: EditText? = null
    var mMinuteEditText: EditText? = null
    var mSetAlarmButton: Button? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_alarm, container, false)
        return view
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mHourEditText = view.findViewById(R.id.hour_edit_text) as EditText?
        mMinuteEditText = view.findViewById(R.id.minute_edit_text) as EditText?
        mSetAlarmButton = view.findViewById(R.id.btn_set_alarm) as Button?

        mSetAlarmButton?.setOnClickListener {

            val hour: Int = mHourEditText?.getText().toString().toInt()
            val minute: Int = mMinuteEditText?.getText().toString().toInt()
            val intent = Intent(AlarmClock.ACTION_SET_ALARM)
            intent.putExtra(AlarmClock.EXTRA_HOUR, hour)
            intent.putExtra(AlarmClock.EXTRA_MINUTES, minute)
            if (hour <= 24 && minute <= 60) {
                startActivity(intent)
            }
        }
    }
}