package com.example.android.budgetscheduler.DataBase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Budget(Amount: Int, Month: String) {
    @PrimaryKey(autoGenerate = true)
    var uid = 0;

    @ColumnInfo(name = "month")
    var Month: String? = Month

    @ColumnInfo(name = "amount")
    var Amount: Int? = Amount
}