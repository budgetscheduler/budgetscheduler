package com.example.android.budgetscheduler.DataBase

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.widget.RecyclerView
import com.example.android.budgetscheduler.R
import kotlinx.android.synthetic.main.insert_new_expense_view.view.*

class Adapter(private val expenses: ArrayList<Expense>) :
    RecyclerView.Adapter<Adapter.CustomViewHolder>() {
    /*var list: List<Int> = arrayListOf()
    var tracker: SelectionTracker<Long>? = null
    init {
        setHasStableIds(true)
    }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.insert_new_expense_view, parent, false)
        return CustomViewHolder(view);
    }

    //    override fun onCreateViewHolder(
//        parent: ViewGroup,
//        viewType: Int
//    ): com.example.tema2java.ui.main.RecyclerAdapter.MyViewHolder? {
//        val textView = LayoutInflater.from(parent.context)
//            .inflate(R.layout.text_view_layout, parent, false) as TextView
//        val myViewHolder
//        return com.example.tema2java.ui.main.RecyclerAdapter.MyViewHolder(textView)
//    }
    override fun getItemCount() = expenses.size

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.bind(expenses[position])
        /*tracker?.let {
            holder.bind(number, it.isSelected(position.toLong()))
        }*/
    }

    inner class CustomViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(user: Expense) {
            view.suma.text = user.Sum.toString()
            view.scop.text = user.Purpose
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private var text: TextView = view.findViewById(R.id.text)

        fun bind(value: Int, isActivated: Boolean = false) {
            text.text = value.toString()
            itemView.isActivated = isActivated
        }

        fun getItemDetails(): ItemDetailsLookup.ItemDetails<Long> =
            object : ItemDetailsLookup.ItemDetails<Long>() {
                override fun getPosition(): Int = adapterPosition
                override fun getSelectionKey(): Long? = itemId
            }
    }

    override fun getItemId(position: Int): Long = position.toLong()
}