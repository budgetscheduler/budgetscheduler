package com.example.android.budgetscheduler.DataBase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Expense(Purpose: String, Sum: Int, Category: String) {
    @PrimaryKey(autoGenerate = true)
    var uid = 0;

    @ColumnInfo(name = "purpose")
    var Purpose: String? = Purpose

    @ColumnInfo(name = "sum")
    var Sum: Int? = Sum

    @ColumnInfo(name = "category")
    var Category: String? = Category
}