package com.example.android.budgetscheduler.Activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.android.budgetscheduler.R
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import kotlinx.android.synthetic.main.fragment_main.*


class MainActivity : AppCompatActivity() {

    var callbackManager: CallbackManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        overridePendingTransition(
            R.anim.slide_in_left,
            R.anim.slide_out_left
        )

        callbackManager = CallbackManager.Factory.create()

        login_button.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                textView.text = "Login Success"

//                textView.text = "Login Success ${result?.accessToken?.userId}" +
//                        "${result?.accessToken?.token}"
            }

            override fun onCancel() {
                textView.text = "Login Canceled"
            }

            override fun onError(error: FacebookException?) {

            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager?.onActivityResult(requestCode, resultCode, data)
    }
}
