package com.example.android.tema2androidechipa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    public static AppDatabase appDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        appDatabase = Room.databaseBuilder(getApplicationContext(), AppDatabase.class,"userdatabase").allowMainThreadQueries().build();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment.newInstance()).commitNow();
        }
    }
}