package com.example.android.tema2androidechipa;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "users")
public class User {


    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "user_first_name")
    private String first_name;

    @ColumnInfo(name = "user_last_name")
    private String last_name;
    public String getFirst_name() {
        return first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }


    public void setFirst_name(String first_name) {

        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }
}
