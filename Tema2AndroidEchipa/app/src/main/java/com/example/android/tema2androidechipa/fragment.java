package com.example.android.tema2androidechipa;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


public class fragment extends Fragment {

    private EditText firstName;
    private EditText lastName;
    private List<User> users;
    private Button addButton;
    private Button deleteButton;
    private View view;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private Recycler adapter;


    public static fragment newInstance() {
        return new fragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_fragment, container, false);
        RefreshRecycle();

        firstName = view.findViewById(R.id.first_name);
        lastName = view.findViewById(R.id.last_name);

        addButton = view.findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstN = firstName.getText().toString();
                String lastN = lastName.getText().toString();
                AddButtonExecute(firstN,lastN);
                RefreshRecycle();
            }
        });

        String fName = firstName.getText().toString();
        String lName = lastName.getText().toString();

        deleteButton = view.findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstN = firstName.getText().toString();
                String lastN = lastName.getText().toString();
                DeleteButtonExecute(firstN, lastN);
                RefreshRecycle();
            }
        });
        return view;
    }
    public void AddButtonExecute(String firstName, String lastName)
    {
        if(firstName.equals("") || lastName.equals(""))
        {
            Toast.makeText(getActivity(),"You didn't fill one of the EditText boxes",Toast.LENGTH_SHORT).show();
        }
        else {
            User user = new User();
            user.setFirst_name(firstName);
            user.setLast_name(lastName);
            if (findUser(user)) {
                Toast.makeText(getActivity(), "This user already exist", Toast.LENGTH_SHORT).show();
            } else {
                MainActivity.appDatabase.dao().addUser(user);
                Toast.makeText(getActivity(), "User added successfully", Toast.LENGTH_SHORT).show();
            }
        }
    }
    public void DeleteButtonExecute(String firstName, String lastName){

        if(firstName.equals("") || lastName.equals(""))
        {
            Toast.makeText(getActivity(),"You didn't fill one of the EditText boxes",Toast.LENGTH_SHORT).show();
        }
        else
        {
            User user = new User();
            user.setFirst_name(firstName);
            user.setLast_name(lastName);
            if(!findUser(user))
            {
                Toast.makeText(getActivity(),"First name or last name are incorrect",Toast.LENGTH_SHORT).show();
            }
            else
            {
                MainActivity.appDatabase.dao().deleteUser(user);
                Toast.makeText(getActivity(),"User deleted successfully",Toast.LENGTH_SHORT).show();
            }
        }
    }

    public boolean findUser(User user) {
        for(User usr : users)
        {
            if(usr.getFirst_name().equals(user.getFirst_name()) && usr.getLast_name().equals(user.getLast_name()))
                return true;
        }
        return false;
    }

    public void RefreshRecycle() {

        recyclerView = view.findViewById(R.id.recycle_view);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        users = MainActivity.appDatabase.dao().getUsers();

        List<String> itemList = new ArrayList<>();
        String infoAboutUser = "";

        for(User usr : users) {
            String firstName = usr.getFirst_name();
            String lastName = usr.getLast_name();
            infoAboutUser = firstName + " " + lastName;
            itemList.add(infoAboutUser);
        }

        adapter = new Recycler(itemList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

}
