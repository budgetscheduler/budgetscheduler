package com.example.tema1.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.tema1.R
import com.example.tema1.activities.MainActivity
import com.example.tema1.activities.SecondActivity
import kotlinx.android.synthetic.main.fragment_third.*

class ThirdFragment : Fragment() {

    companion object {
        fun newInstance() = ThirdFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_third, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_to_final_fragment.setOnClickListener {
            toFinalFragment()
        }

        btn_remove_F1A2.setOnClickListener {
            removeF1A2()
        }
        btn_close_activity.setOnClickListener {
            activity!!.finish()
        }

    }

    fun toFinalFragment(){
        val transaction = fragmentManager!!.beginTransaction()
        transaction.replace(R.id.primary_frame_s_session, FourthFragment.newInstance())
        transaction.addToBackStack(ThirdFragment.toString())
        transaction.commit()
    }

    fun removeF1A2()
    {

    }
}
