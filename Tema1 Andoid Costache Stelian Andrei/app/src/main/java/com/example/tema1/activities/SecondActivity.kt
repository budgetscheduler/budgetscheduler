package com.example.tema1.activities

import android.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.tema1.R
import com.example.tema1.fragments.SecondFragment
import kotlinx.android.synthetic.main.fragment_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        setFirstFragment()
    }

    fun setFirstFragment() {
        val fragment: SecondFragment = SecondFragment.newInstance()

        val transaction =supportFragmentManager.beginTransaction()
        transaction.add(R.id.primary_frame_s_session, fragment)
        transaction.addToBackStack(SecondFragment.toString())
        transaction.commit()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}
