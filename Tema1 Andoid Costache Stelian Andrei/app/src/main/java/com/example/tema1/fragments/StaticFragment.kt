package com.example.tema1.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.tema1.R
import com.example.tema1.activities.MainActivity
import com.example.tema1.activities.SecondActivity
import kotlinx.android.synthetic.main.static_fragment.*

class StaticFragment : Fragment() {
    companion object {
    }

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?,  savedInstanceState: Bundle?)
            = inflater.inflate(R.layout.static_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_static.setOnClickListener {
            goToNextActivity()
        }
    }
    fun goToNextActivity()
    {
        val intent = Intent(activity, SecondActivity::class.java)
        startActivity(intent)
    }
}
