package com.example.tema1.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.tema1.R
import kotlinx.android.synthetic.main.activity_main.*

import androidx.fragment.app.FragmentActivity
import kotlinx.android.synthetic.main.fragment_f1_a1.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_go_to_activity_two.setOnClickListener{
            goToSessionActvity()
        }
    }

    private fun goToSessionActvity() {
        val intent = Intent(this, SecondActivity::class.java)
        startActivity(intent)
    }
}

