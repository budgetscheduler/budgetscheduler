package com.example.tema1.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import com.example.tema1.R
import kotlinx.android.synthetic.main.fragment_f1_a2.*

class F1A2Fragment : Fragment() {

    companion object {
        fun newInstance(): F1A2Fragment = F1A2Fragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val v = inflater.inflate(R.layout.fragment_f1_a2, container, false)

       /** val btn = v.findViewById<View>(R.id.btn_go_to_fragment_two) as Button

        btn.setOnClickListener {
            val fragment = F1A2Fragment()
            val fragmentManager = activity!!.supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.f2a2_session, fragment)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }
        // Inflate the layout for this fragment */
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}
