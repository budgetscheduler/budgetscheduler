package com.example.tema1.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.tema1.R
import com.example.tema1.fragments.F1A2Fragment
import com.example.tema1.fragments.F2A2Fragment
import kotlinx.android.synthetic.main.fragment_f1_a2.*


class SecondActivity : AppCompatActivity() {
    companion object {
        enum class FragmentTags(val value: String) {
            TAG_FRAGMENT_F1A2("TAG_FRAGMENT_F1A2"),
            TAG_FRAGMENT_F2A2("TAG_FRAGMENT_F2A2")
        }
    }

    var currentFragmentTag: FragmentTags? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        btn_go_to_fragment_two.setOnClickListener {
            changeFragment()
        }
    }

    fun changeFragment() {
        currentFragmentTag = when (currentFragmentTag) {
            null -> FragmentTags.TAG_FRAGMENT_F1A2
            FragmentTags.TAG_FRAGMENT_F1A2 -> FragmentTags.TAG_FRAGMENT_F2A2
            FragmentTags.TAG_FRAGMENT_F2A2 -> Companion.FragmentTags.TAG_FRAGMENT_F1A2
        }
        replaceFragment(currentFragmentTag)
    }

    private fun replaceFragment(TAG: FragmentTags?) {
        TAG?.run {
            val fragment = when (this) {
                FragmentTags.TAG_FRAGMENT_F1A2 ->
                    F1A2Fragment.newInstance()
                FragmentTags.TAG_FRAGMENT_F2A2 ->
                    F2A2Fragment.newInstance()
            }

            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.f1a2_session, fragment as Fragment, this.value)
            transaction.addToBackStack(this.value)
            transaction.commit()
        }
    }
}

